# Custom scripts
This repository provides two python scripts to ease the pain of dealing with the
solutions handed in by students in Ilias. The script `get_testate_upload.py`
collects the solutions of all students belonging to a certain exercise class and
copies them into a destination directory. The script `compile_all_testate.py`
compiles all files having a `.cc` or `.cpp` suffix within a certain directory.

# Usage
**Running any of the commands below or executing any of the programs within this repository is completely at your own risk!**
1. Obtain (for a certain exercise sheet) the solutions handed in by all
students using the button "Alle Abgaben herunterladen" in the "Abgaben and Noten"
section.
2. Extract the `zip` archive you downloaded.
3. Edit the file `get_testate_upload.py` and add the u-Kürzel of all students
of your exercise class into the list `UKUERZEL_LIST`.
4. You can use

        python3 get_testate_upload.py "SOURCE_DIRECTORY" (--dest="DESTINATION_DIRECTORY")

    to copy the files of those students into DESTINATION_DIRECTORY. These files
    will be given the u-Kürzel as a prefix in order to distinguish them.
    E.g., the command

        python3 get_testate_upload.py "Blatt 1/" --dest="blatt1"

    will copy (among others) the file "Blatt 1/Abgaben/John_Doe_uxxxx_1000000/test.cc"
    to "blatt1/uxxxx_test.cc".
    This should save you some navigation between directories belonging to different
    students.
5. Look through the solutions handed in by the students. In particular, you should make sure that no harm might be done by compiling the programs or executing the compiled ones.
6. If you are sure that your machine is safe, you can compile all `.cc` and `.cpp` files within a certain directory with one command:

        python3 compile_all_testate.py "DIRECTORY"

7. The executable files will be names just as the source files, except for the suffix, which is omitted, e.g. `uxxxx_test.cc` -> `uxxxx_test`.
