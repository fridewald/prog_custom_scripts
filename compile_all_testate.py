"""
Compiles all files in DIRECTORY that have ".cc" or ".cpp" as suffix
This might harm your machine. Do not use this program without knowing what the C++ files do.

Example usage
python3 compile_all_testate.py "blatt1"

USE AT OWN RISK
"""

import os
import subprocess
import argparse


# Process input arguments
parser = argparse.ArgumentParser(allow_abbrev=True)
parser.add_argument("directory", help="Directory containing the files you want to compile")
args = parser.parse_args()
DIRECTORY = args.directory

file_list = os.listdir(DIRECTORY)
for f in file_list:
    try:
        if (f.split(".")[1] == "cc" or f.split(".")[1] == "cpp"):
            fname = os.path.join(DIRECTORY, f)
            subprocess.call(["g++", fname, "-o", fname.split(".")[0]])
            print("Compiled " + fname + " -> " + fname.split(".")[0])
    except IndexError:
        continue
