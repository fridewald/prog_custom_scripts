"""
Extracts the files of the students in your exercise class

Example usage:
python3 get_testate.py "Blatt 1" --dest="blatt1"

USE AT YOUR OWN RISK
"""

import os
import click
import sys
import shutil
from datetime import datetime
import argparse


# List of u-Kuerzel of your students
UKUERZEL_LIST = ["uxxxx", "uyyyy", "uzzzz"]
print(UKUERZEL_LIST)


# Process input arguments
parser = argparse.ArgumentParser(allow_abbrev=True)
parser.add_argument("source", help="Source directory containing the files you want to extract")
parser.add_argument("--dest", help="Destination directory for the extracted files")
args = parser.parse_args()
SOURCE_DIRECTORY = args.source
DEST_DIRECTORY = args.dest

# If no destination directory is given, use the current time as directory name
if DEST_DIRECTORY is None:
    DEST_DIRECTORY = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


# Try to create the destination directory
try:
    os.makedirs(DEST_DIRECTORY)
except FileExistsError:
    print("The destination directory already exists, and so may its content. If you continue, you might override the existing content.")
    if not click.confirm("Do you want to continue? ", default=False):
        print("Existing with grace.")
        sys.exit(0)

list_of_directories = [f[0] for f in os.walk(SOURCE_DIRECTORY)]


# Generate list of all directories belonging to the students defined in UKUERZEL_LIST
# Copy files
dir_list = []
for d in list_of_directories:
    for u in UKUERZEL_LIST:
        if u in d:
            dir_list.append(d)
            all_files = os.listdir(d)
            for f in all_files:
                fname = os.path.join(d, f)
                # Copy files prepending the u-Kuerzel as prefix
                try:
                    shutil.copy(fname, DEST_DIRECTORY+"/"+u+"_"+f)
                except FileNotFoundError:
                    print(fname+" does not seem to exist. Omitting.")
                    continue

print("Copied contents of the following directories to " + DEST_DIRECTORY)
print(dir_list)
print("Finished.")
